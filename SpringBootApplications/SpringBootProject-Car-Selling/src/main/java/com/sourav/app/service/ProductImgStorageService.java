package com.sourav.app.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.sourav.app.model.Product;
import com.sourav.app.model.ProductImg;
import com.sourav.app.repository.ProductImgRepository;
import com.sourav.app.repository.ProductRepository;

@Service
public class ProductImgStorageService {

    @Autowired
    private ProductImgRepository dbFileRepository;
    
    @Autowired
	ProductRepository productRepo;

    public Product storeFile(MultipartFile file,Long productId) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
       	Product productRetrived = productRepo.findById(productId).orElse(null);   
       	if(productRetrived!=null) {
       		
       	}
        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new RuntimeException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            ProductImg dbFile = new ProductImg(fileName, file.getContentType(), file.getBytes());
            productRetrived.getProductPicture().add(dbFile);
            productRepo.save(productRetrived);
            return productRetrived;
        } catch (IOException ex) {
            throw new RuntimeException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public ProductImg getFile(Long fileId) {
        return dbFileRepository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("File not found with id " + fileId));
    }
}