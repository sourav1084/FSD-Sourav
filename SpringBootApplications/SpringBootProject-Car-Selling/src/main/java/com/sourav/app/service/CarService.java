package com.sourav.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sourav.app.model.Car;
import com.sourav.app.repository.CarRepository;

@Service
public class CarService {

	@Autowired
	private CarRepository cRepo;

	public void addCar(Car c) {
		cRepo.save(c);
	}

	public List<Car> getAllCar() {

		return (List<Car>) cRepo.findAll();

	}
	public void delteCar(Car del) {
		cRepo.delete(del);
	}

}
