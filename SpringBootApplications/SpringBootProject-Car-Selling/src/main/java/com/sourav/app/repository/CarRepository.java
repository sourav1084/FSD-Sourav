package com.sourav.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sourav.app.model.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
	

}
