package com.sourav.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootProjectCarSellingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootProjectCarSellingApplication.class, args);
		
		
	}

}
