package com.sourav.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sourav.app.model.Cart;
import com.sourav.app.model.User;


@Repository
public interface UserRepository extends JpaRepository<User,String>{

	@Query("SELECT u FROM User u WHERE u.username = ?1 and u.password = ?2")
	User loginCheck(String username, String password);
	
	@Query("SELECT u FROM User u WHERE u.cart = ?1")
	User getUserByCarId(Cart cartId);

}
