package com.sourav.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sourav.app.model.ProductImg;

@Repository
public interface ProductImgRepository extends JpaRepository<ProductImg, Long> {

}
