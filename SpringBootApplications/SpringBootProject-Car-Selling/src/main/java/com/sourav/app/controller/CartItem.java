package com.sourav.app.controller;
public class CartItem {
	
	private Long cartId;
	private String userName;
	private String productId;
	
	public CartItem() {}
	
	public CartItem(Long cartId, String userName, String productId) {
		super();
		this.cartId = cartId;
		this.userName = userName;
		this.productId = productId;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "CartItem [cartId=" + cartId + ", userName=" + userName + ", productId=" + productId + "]";
	}
}

