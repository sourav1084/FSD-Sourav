package com.sourav.app.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Cart {
 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cart_id;	
	
	@ManyToMany
	private Set<Product> productList;

	@Override
	public String toString() {
		return "Cart [id=" + cart_id + "]";
	}

	public Long getCart_id() {
		return cart_id;
	}

	public void setCart_id(Long cart_id) {
		this.cart_id = cart_id;
	}

	public Set<Product> getProductList() {
		return productList;
	}

	public void setProductList(Set<Product> productList) {
		this.productList = productList;
	}

   
	
}
