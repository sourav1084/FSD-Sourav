package fsd.sb.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fsd.sb.model.User;

@RestController
public class UserController {

	private static ArrayList<User> ulist = new ArrayList<>();
	
	static {
		System.out.println("Calling Static Block...");
		addDefaultUsers();
	}
	
	private static void addDefaultUsers() {
		
		User u1 = new User ("user1","user1@gmail.com",100L);
		User u2 = new User ("user2","user2@gmail.com",100L);
		User u3 = new User ("user3","user3@gmail.com",100L);
		
		ulist.add(u1);
		ulist.add(u2);
		ulist.add(u3);
		
		
		
	}
	
	@GetMapping("/users")
	public ArrayList<User> getAllUser(){
		return ulist;
	}
	
	@PostMapping("/adduser")
	public boolean addUser(@RequestBody User u) {
		return ulist.add(u);
	}
	
	@DeleteMapping("/deleteuser")
	public boolean deleteUser(@PathVariable(value = "uid")Long uid) {
		int index = -1;
		
		for(int i=0;i<ulist.size();i++) {
			if(ulist.get(i).getId()==uid) {
				index = i;
			}
		}
		
		if(index !=-1) {
			ulist.remove(index);
			return true;
		}else {
			return false;
		}

	}
	
	
}
