package com.sourav.sb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.sourav.sb.entity.Student;
import com.sourav.sb.repository.StudentRepository;
import com.sourav.sb.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	private StudentRepository studentRepository;


	@Override
	public List<Student> getAllStudents() {
		
		
		return studentRepository.findAll();
	}


	@Override
	public Student addStudent(@RequestBody Student s) {
		return studentRepository.save(s);
	}

	
}
