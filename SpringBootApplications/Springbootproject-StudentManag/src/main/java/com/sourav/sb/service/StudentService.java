package com.sourav.sb.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.sourav.sb.entity.Student;

public interface StudentService {
	
	public List<Student> getAllStudents();
	
	public Student addStudent(@RequestBody Student s);
}
