package com.sourav.sb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Repository;

import com.sourav.sb.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student , Long> {

	
	
	
	
}
