package com.fsd.sb.controller;

import java.util.Date;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MsgController {
	
	@GetMapping("/hi")
	public String hiMsg() {
		return "Hi Everyone";
	}
	
	@GetMapping("/v2/hi")
	public String hiMsg2() {
		return "Hi Everyone";
	}
	
	@GetMapping("/date")
	public String getDate() {
		return "Today's date is "+new Date();
	}
	
	@GetMapping("/mymsg")
	public String msg(@PathParam(value = "msg") String msg) {
		return "Hello "+msg;
	}
	
	//hi user id: 200
	@GetMapping("/user")
	public String getUserById(@PathParam(value = "id") String id) {
		if(id==null) {
			return "Please provide user id";
		}else {
			return "hi user id :"+id;
		}
	}
	
	@GetMapping("/add")
	public String add(@PathParam(value="n1") String n1,@PathParam(value="n2") String n2 ) {
		Integer result = Integer.valueOf(n1) + Integer.valueOf(n2);
		return "Sum="+result;
	}
	
	@GetMapping("/v2/user/{id}")
	public String getUserById2(@PathVariable(value = "id") String id) {
		if(id==null) {
			return "Please provide user id";
		}else {
			return "hi user id :"+id;
		}
	}
	
	@GetMapping("/v1/login")
	public boolean login(@PathParam(value="uname")String uname,@PathParam(value="pass")String pass) {
		System.out.println("uname="+uname + " and pass "+pass);
		if(uname.equalsIgnoreCase("vishal") && pass.equals("fsd")) {
			return true;
		}else {
			return false;
		}
		
	}
	
	@PostMapping("/v2/login")
	public boolean loginv2(@PathParam(value="uname")String uname,@PathParam(value="pass")String pass) {
		System.out.println("uname="+uname + " and pass "+pass);
		if(uname.equalsIgnoreCase("vishal") && pass.equals("fsd")) {
			return true;
		}else {
			return false;
		}
		
	}

}

