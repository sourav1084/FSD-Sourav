package com.sourav.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class User implements Serializable{
	
	private long UId;
	@Column
	private String Uname;
	
	@Column
	private String Uemail;
	
	@Column
	private String Uaddress;
	
	@Column
	private String Ugender;
	
	@Column
	@Id
	private int Uphone;
	
	@Column
	@Id
	private int UAge;
	
	public User() {}

	public long getUId() {
		return UId;
	}
    public void setUId(long UId) {
    	UId = UId;
    }
	
	
	public String getUname() {
		return Uname;
	}

	public void setUname(String uname) {
		Uname = uname;
	}

	public String getUemail() {
		return Uemail;
	}

	public void setUemail(String uemail) {
		Uemail = uemail;
	}

	public String getUaddress() {
		return Uaddress;
	}

	public void setUaddress(String uaddress) {
		Uaddress = uaddress;
	}

	public String getUgender() {
		return Ugender;
	}

	public void setUgender(String ugender) {
		Ugender = ugender;
	}

	public int getUphone() {
		return Uphone;
	}

	public void setUphone(int uphone) {
		Uphone = uphone;
	}

	public int getUAge() {
		return UAge;
	}

	public void setUAge(int uAge) {
		UAge = uAge;
	}

	public User(String uname, String uemail, String uaddress, String ugender, int uphone, int uAge) {
		super();
		Uname = uname;
		Uemail = uemail;
		Uaddress = uaddress;
		Ugender = ugender;
		Uphone = uphone;
		UAge = uAge;
	}

}
