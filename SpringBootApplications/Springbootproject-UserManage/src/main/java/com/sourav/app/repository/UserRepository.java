package com.sourav.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sourav.app.model.User;

@SuppressWarnings("hiding")
@Repository
public interface UserRepository extends CrudRepository<User,Long>{

}


