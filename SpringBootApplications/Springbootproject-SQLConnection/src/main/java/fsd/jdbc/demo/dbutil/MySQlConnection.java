package fsd.jdbc.demo.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQlConnection {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/demo2";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "mysql";
	
	private static Connection conn = null;

	public static Connection getMySqlConnection() {	
		if(conn==null) {
			try {
				//Register JDBC driver for older mysql & older drivers
				//Class.forName("com.mysql.jdbc.Driver");
				//  Open a connection
				System.out.println("Connecting to database...");
				conn = DriverManager.getConnection(DB_URL, USER, PASS);
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			}finally {
				if(conn == null ) {
					System.out.println("##########Error connection to database######");
					return null;
				}else {
					return conn;
				}			
			}
		}else {
			return conn;
		}
		
	}
	
	public static boolean closeConnection() {
		if(conn!=null) {
			try {
				conn.close();
				System.out.println("########Connection closed###########");
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("########Connection object is not created it is NULL###########");
		return false;		
	}
	
}

