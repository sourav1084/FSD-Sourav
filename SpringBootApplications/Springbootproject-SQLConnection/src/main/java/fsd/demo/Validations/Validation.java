package fsd.demo.Validations;

public class Validation {
	
	/**
	 * 
	 * A valid user name must not be null 
	 * must not be empty
	 * contains alteast 4 characters
	 */
	public static boolean isValidUserName(String username) {
		
		if(username == null) {
			return false;
		}else if(username.length() == 0 || username.length() < 4) {
			return false; 
		}else {
			return true;
		}
	}
	
	public static boolean isValidPassword(String password) {
		if(password == null) {
			return false;
		}else if(password.length() == 0 || password.length() < 4) {
			return false; 
		}else {
			return true;
		}
	}
	
	public static boolean isValidEmail(String email) {
		return true;
	}

}
