package com.sourav.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sourav.app.model.Course;
import com.sourav.app.services.CourseService;

@RestController
public class Controller {

	@Autowired
	private CourseService cou;

	@GetMapping("/hi")
	public String home() {

		return "This is Sourav";
	}

	// get the courses
	@GetMapping("/getcou")
	public List<Course> getCoursesList() {

		return cou.getCourses();

	}

	// adding course
	@PostMapping("/courses")
	public String addCourse(@RequestBody Course cadd) {
		cou.addCourse(cadd);
		return "added to db";

	}

	// Update Course Using Put request
	@PutMapping("/update")
	public String updateCourse(@RequestBody Course cupd) {

		cou.addCourse(cupd);

		return "updated";
	}

	// Delete Course using delete
	@DeleteMapping("/del")
	public String delete(@RequestBody Course de) {
		
		cou.delete(de);
		return "deleted";
	}

}
