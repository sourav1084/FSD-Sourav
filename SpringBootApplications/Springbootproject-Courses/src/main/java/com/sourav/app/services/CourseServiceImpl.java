package com.sourav.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sourav.app.model.Course;
import com.sourav.app.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepo;

	public CourseServiceImpl() {

	}

	@Override
	public List<Course> getCourses() {

		return courseRepo.findAll();
	}

	@SuppressWarnings("deprecation")
	@Override
	public Course getCourse(long courseId) {

		return courseRepo.getOne(courseId);
	}

	@Override
	public Course addCourse(Course course) {

		courseRepo.save(course);
		return course;
	}

	@Override
	public Course updateCourse(Course course) {
		courseRepo.save(course);
		return course;
	}

	@Override
	public void delete(Course de) {
		// TODO Auto-generated method stub

	}

}
