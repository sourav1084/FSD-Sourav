package com.sourav.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductManagementsApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(ProductManagementsApplication.class, args);
	}

}
