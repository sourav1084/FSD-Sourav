package com.sourav.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sourav.app.model.Course;

public interface CourseRepository extends JpaRepository<Course, Long>{
	
	

}
