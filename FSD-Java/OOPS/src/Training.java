public class Training {
	//instance variable or Object properties
	// out side all the method of the class, directly inside the class
	// NOT INSIDE ANY METHOD
	// Every Object will have a copy of it
	private int tId;
	private String trainingName;
	
	//static variable
	// All the object will share it
	//only one copy of static variable will be shared by all the objects
	public static String trainerName;
	
	public Training() {
		//default constructor: No args constructor
		System.out.println("Default Constructor");
	}
	
	public Training(int tId, String trainingName) {
        System.out.println("2 args constructor");
		this.tId = tId;
		this.trainingName = trainingName;
	}
	
	

}

