package Inheritence;

public class Employee{
	
	String Emp_Name;
	int Emp_Id;
	int Emp_sal;
	
	public Employee() {}

	public Employee(String Emp_Name,int Emp_Id) {
		this.Emp_Id =Emp_Id;
		this.Emp_Name= Emp_Name;
		
	}
	public Employee(String Emp_Name,int Emp_Id,int Emp_sal) {
		this(Emp_Name,Emp_Id);
		this.Emp_Name= Emp_Name;
		
	}

	public String getEmp_Name() {
		return Emp_Name;
		
	}

	public void setEmp_Name(String emp_Name) {
		Emp_Name = emp_Name;
		
	}

	public int getEmp_Id() {
		return Emp_Id;
		
	}

	public void setEmp_Id(int emp_Id) {
		Emp_Id = emp_Id;
		
	}
}