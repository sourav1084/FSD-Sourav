package Inheritence;

public class Car {
	
	    	   
	String Brand;
	String Type;
	int Speed;
	public Car() {}
	
	public Car(String Brand,String Type, int Speed) {
	
	this.Brand=Brand;
	this.Type=Type;
	this.Speed=Speed;
	}

	@Override
	public String toString() {
		return "Car [Brand=" + Brand + ", Type=" + Type + ", Speed=" + Speed + "]";
	}

	public String getBrand() {
		return Brand;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public int getSpeed() {
		return Speed;
	}

	public void setSpeed(int speed) {
		Speed = speed;
	}

	
	

}
