 package objects;

public class Account {
	
     private int aId;
     private String custName;
     
     public static String bankName;
     
     public Account() {
    	 System.out.println("Account class default constructor");
     }
     
     public Account(int aId,String custName) {
    	 System.out.println("Account class 2 para constructor");
    	 if(aId < 0) {
    		 System.out.println("Invalid aId");
    	 }else {
    		 this.aId = aId;
    	 }
    	 this.custName = custName;
     }
     
     public void showAccDetails() {
    	 System.out.println(this.aId + "-"+this.custName + "-"+bankName);
     }
}

