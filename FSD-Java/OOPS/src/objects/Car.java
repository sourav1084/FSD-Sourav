package objects;

public class Car {
	
	private String ModelName;
	private int year;
	
	public Car() {}
	
	public Car(String ModelName, int year) {
		
		this.ModelName=ModelName;
		this.year=year;
	}

	/**
	 * @return the modelName
	 */
	public String getModelName() {
		return ModelName;
	}

	/**
	 * @param modelName the modelName to set
	 */
	public void setModelName(String modelName) {
		ModelName = modelName;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param string the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	public void setYear(String ModelName) {
		// TODO Auto-generated method stub
		
	}
	

}
