package objects;

public class Book {
	
		   // instance variables
		   private String title;
		   private String author;
		   private double price;
		   private String name;
		   private String bookType;

		   // constructor
		  


		public Book(String title, String author, int price, String name, String bookType) {
			this.title = title;
			this.author=author;
			this.price = price;
			this.name = name;
			this.bookType= bookType;
		}

		public String getTitle() {
			return title;
		}

		/**
		 * @param title the title to set
		 */
		public void setTitle(String title) {
			this.title = title;
		}

		/**
		 * @return the author
		 */
		public String getAuthor() {
			return author;
		}

		/**
		 * @param author the author to set
		 */
		public void setAuthor(String author) {
			this.author = author;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBookType() {
			return bookType;
		}

		public void setBookType(String bookType) {
			this.bookType = bookType;
		}

		/**
		 * @return the price
		 */
		public double getPrice() {
			return price;
		}

		/**
		 * @param price the price to set
		 */
		public void setPrice(double price) {
			this.price = price;
		}
		
		


		
		public String toString() {
			return "Book [title=" + title + ", author=" + author + ", price=" + price + ", name=" + name + ", bookType="
					+ bookType + "]";
		}


		public void showDetails( ) {
			System.out.println(this.author + "-"+this.title + "-"+this.price);
			
		}
		}


