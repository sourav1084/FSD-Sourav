package objects;

import java.util.Scanner;

public class Emp {
	// which is the best data type of storing unique items or primary : long
	// properties of the class or INSTANCE VARIABLES
	// 
	public long empId;
	public String empName;
	public String empDept;
	private double empSal;
	public String empCity;	
	private String empPwd;
	
	public Emp() {}
	
  
	public Emp(long empId, String empName, String empDept, String empCity, double empSal, String empPwd) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empDept = empDept;
		this.empSal = empSal;
		this.empCity = empCity;
		this.empPwd = empPwd;
	}
	
	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpDept() {
		return empDept;
	}
	public void setEmpDept(String empDept) {
		this.empDept = empDept;
	}
	public double getEmpSal() {
		return empSal;
	}
	public void setEmpSal(double empSal) {
		this.empSal = empSal;
	}
	public String getEmpCity() {
		return empCity;
	}
	public void setEmpCity(String empCity) {
		this.empCity = empCity;
	}
	public String getEmpPwd() {
		return empPwd;
	}
	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}
	
	@Override
	public String toString() {
		return "Emp [empId=" + empId + ", empName=" + empName + ", empDept=" + empDept + ", empSal=" + empSal
				+ ", empCity=" + empCity + ", empPwd=" + empPwd + "]";
	}
	public void showDetails() {

       System.out.print(getEmpName()+" , "+getEmpId()+" , "+getEmpSal()+" , "+getEmpDept()+" , "+getEmpCity()+" , "+getEmpPwd()+" . ");
       
	}
}

