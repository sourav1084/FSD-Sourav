package Abstraction;

abstract class Animal {

	abstract public void walk();
	
	Animal(){
		System.out.println("Creating new Animal");
	}

	public void eat() { 
           
		System.out.println("Animal Eat ");
	}
}

class Horse extends Animal {

	public void walk() {

		System.out.println("walk on 5 legs");
	}  
	Horse(){
		System.out.println("Created Horse");
	}
}

class Chicken extends Animal {

	public void walk() {
		System.out.println("walk on 2 legs");
	}
}

public class OOPS { 

	public static void main(String[] args) {

		Horse horse = new Horse();
		horse.walk();
		//Animal animal = new Animal();//Cannot instantiate the type Animal
//		animal.walk();
		horse.eat();

	}

}