package Polymorphism;

class Student1 {
	
	String name;
	int age;
	
	public void printInfo(String name) {
		System.out.println(name);
		
	}
	public void printInfo(int age) {
		System.out.println(age);
		
	}
	public void printInfo(String name,int age) {// OVERLOADING
		System.out.println(name +" "+ age);
		
	}
	public void printInfo(int age,String name) {// OVERLOADING
		System.out.println(age +" "+ name);
		
	}
}
	
    public class Overload{
    	
	public static void main(String[] args) {
		
		Student1 s1 =new Student1();
		s1.name="James";
		s1.age=21;
		
		
		s1.printInfo(s1.age,s1.name);
		s1.printInfo(s1.name,s1.age);
		
	}

    }

