package ObjectOriented;


class Pen{
	String color;
	String type;//gel/ball pen
	
	public void write() {
		System.out.println("Write something");
	}
	
	public void printcolor() {
		this.color=color;
		 
	}
   public void printtype() {
	   System.out.println(this.type);
   }


}

public class OOPS{
	public static void main(String[] args) {
		
		Pen pen1 = new Pen();
		pen1.color ="blue";
		pen1.type="gel";
		
		Pen pen2 =new Pen();
		pen2.color="white";
		pen2.type="ball";
		
		pen1.printcolor();
		pen2.printcolor();
		pen2.printtype();
	}
	
	
}