package GitPrograms;

public class Factorial {

	public static void main(String[] args) {
		//  Factorial of a given number 
		
		int num = 5;
		int factCount =1;
		
		
		for(int i=1;i<=num;i++) {
			
			factCount = factCount*i;			
		}
		System.out.println("Factorial of "+ num + " is "+ factCount);

	}

}
