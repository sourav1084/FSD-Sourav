package GitPrograms;

public class Swap2Num {

	public static void main(String[] args) {
		
		int n1 = 10;
	    int n2 = 20;
	    int temp;
	    
	    System.out.println("Before Swaping :" +"n1=" + n1 +" "+"n2="+  n2);
	    
	   
	    temp = n1;
	    n1 = n2;
	    n2 = temp;
	    System.out.println("After Swaping :" +"n1=" + n1 +" "+"n2="+  n2);
	    
	    
	    
	    System.out.println("Without using Temperory Variable");
	    
	    int num1 = 10;
	    int num2 = 20;
	    
	    System.out.println("Before Swaping :" +"n3=" + num1 +" "+"n4="+  num2);
	    
	    num1=num1+num2;
	    num2=num1-num2;
	    num1=num1-num2;
	    
	    System.out.println("After Swaping :" +"n3=" + num1 +" "+"n4="+  num2);
	    

	}

}
