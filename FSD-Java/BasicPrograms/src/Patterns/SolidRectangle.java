package Patterns;

public class SolidRectangle {
	public static void main(String args[]) {
		int noofrows = 6;
		int noofcols = 6;
//	       int counter=0;

		for (int i = 1; i < noofrows; i++) {

			for (int j = 1; j < noofcols; j++) {
//	        	   counter++;
				if (i == j) {
					System.out.print(" * ");
				}
				if (j >= i) {
					System.out.print(" @ ");
				} else {
					System.out.print(" # ");

				}
			}
			System.out.println();
		}
	}
}
