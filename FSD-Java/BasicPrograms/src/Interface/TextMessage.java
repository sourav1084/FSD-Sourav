package Interface;

interface Message {
	
	 int num = 30;

	 abstract void type();
}

public class TextMessage implements Message {
	
	@Override
	public void type() {
	
		System.out.println("Hello");
	}

	public static void main(String args[]) {

		Message obj = new TextMessage();
		obj.type();
		System.out.println(obj.num);
		
	}
}
