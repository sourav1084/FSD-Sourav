package Tests;

public class Electronics {

	
	String Brand;
	String Type;
	int price;
	public Electronics() {
		
	}
	public Electronics(String brand, String type, int price) {
		super();
		Brand = brand;
		Type = type;
		this.price = price;
	}
	@Override
	public String toString() {
		return "Electronics [Brand=" + Brand + ", Type=" + Type + ", price=" + price + "]";
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
		


}
