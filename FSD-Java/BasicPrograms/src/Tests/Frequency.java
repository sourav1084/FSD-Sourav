package Tests;

public class Frequency {

	public static void main(String[] args) {

		String s = "aaaadddvvccvvvbcd";
		for (int i = 0; i < s.length(); i++) {

			int count = 1;
			for (int j = 0; j < s.length(); j++) {

				if (s.charAt(i) == s.charAt(j) && i!=j) {
					count++;				
				}						
			}	
			
			System.out.println(s.charAt(i) + "--" + count);
		}
	}
}
