package Tests;

abstract class X {
	
	abstract void disp();

}

 class Z extends X {
	
	void disp() {
		System.out.println("Abstract method called");
	}
	
	public static void main(String[] args) {
		
		Z obj = new Z ();
		obj.disp();
	}
}
