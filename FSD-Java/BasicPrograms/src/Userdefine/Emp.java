package Userdefine;

public class Emp {

	private long empId;
	private String empName;
	private int empSal;

	public Emp() {

	}

	public Emp(long empId, String empName,int empSal) {
	
		this.empId = empId;
		this.empName = empName;
		this.empSal=empSal;
	}



	public long getEmpId() {
		return empId;
	}

	public void setEmpId(long empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getEmpsal() {
		return empSal;
	}

	public void setEmpsal(int empsal) {
		this.empSal = empsal;
	}

	@Override
	public String toString() {
		return "Emp [empId=" + empId + ", empName=" + empName + ", empsal=" + empSal + "]";
	}

}
