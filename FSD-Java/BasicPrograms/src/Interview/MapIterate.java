package Interview;
import java.util.HashMap;
import java.util.Map;

public class MapIterate {

	public static void main(String[] args) {

		Map<String, Integer> map = new HashMap<String, Integer>();

		map.put("Sourav", 101);
		map.put("raj", 102);
		map.put("ramu", 103);
		map.put("Subham", 104);

		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String key = (String) entry.getKey();

			Integer val = entry.getValue();

			System.out.println(key + " = " + val);

		}

	}
}
