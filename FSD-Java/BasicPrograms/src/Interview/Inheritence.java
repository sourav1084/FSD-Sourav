package Interview;

public class Inheritence {

	void Atest() {
		System.out.println("hello");
	}

	//here it is final and it cannot be changed by other class or inheritence
	final void Atest2() {
		System.out.println("Good Morning");
	}
	
	
	class B extends Inheritence{
	//here this method is final and it cannot be changed by other class or inherit
		 void Atest2() {
			System.out.println("hi");
		}
		
	}
	
	

	public static void main(String[] args) {
		
		Inheritence obj = new Inheritence();
		obj.Atest2();
		
	}

}
