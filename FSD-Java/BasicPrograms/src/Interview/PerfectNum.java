package Interview;

public class PerfectNum {

	public static void main(String[] args) {

		int num = 9;
		int count = 1;
		for (int i = 2; i < num; i++) {

			if (num % i == 0) {
				count = count + i;
			}
		}
		if (num == count) {
			System.out.println(num + " number is perfect");
		} else {
		 	System.out.println(num + " number is not perfect");
		}

	}
}
