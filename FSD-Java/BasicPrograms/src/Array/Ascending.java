package Array;

public class Ascending {

	public static void main(String args[]) {

		int[] arr = { 88, 2, 9, -4, 5 };

		for (int i = 0; i < arr.length; i++) {

			for (int j = i + 1; j < arr.length; j++) {

				if (arr[j] < arr[i]) {
					
					int temp = arr[i];
					
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		
		System.out.print(arr[i]+",");
			}

		}
	}

