package Arithmatic;

public class Greatest {

	public static void main(String[] args) {

		int n1 = 5, n2 = 10, greatest;

		System.out.println("First num: " + n1);
		System.out.println("Second num: " + n2);

		greatest = (n1 > n2) ? n1 : n2;

		System.out.println("Greatest is = " + greatest);
	}
}
