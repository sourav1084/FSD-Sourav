package Arithmatic;

public class Display1to20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int rows = 4;
		int cols = 5;
		int count = 0;
		for (int i = 1; i <= cols; i++) {

			for (int j = 1; j <= rows; j++) {
				count++;

				if (count < 10) {
					System.out.print("0" + count + " ");
				} else {
					System.out.print(count + " ");
				}
			}
			System.out.println();
		}
	}

}
