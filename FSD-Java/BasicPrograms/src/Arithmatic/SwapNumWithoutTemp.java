package Arithmatic;

public class SwapNumWithoutTemp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1=30,num2=20;
		System.out.println("Before swap: num1 = "+num1+" and\n             num2 = "+num2);
		num1 = num1+num2;   //30+20 = 50
		num2 = num1-num2;   //50-20 = 30		
		num1 = num1-num2;   //50-30 = 20
		System.out.println("After swap: num1 = "+num1+" and\n             num2 = "+num2);
		
		
		

	}

}
