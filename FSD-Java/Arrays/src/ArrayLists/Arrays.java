package ArrayLists;

import java.util.ArrayList;
import java.util.Collections;

public class Arrays {

	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<Integer>();

		/* ADDING Elements */
		list.add(2);
		list.add(3);
		list.add(8);
		list.add(4);

		System.out.println(list);

		/* Getting Elements */
		System.out.println(list.get(0));

		/* ADDING Elements IN Between */

		list.add(1, 9);

		System.out.println(list);

		/* Setting Elements */

		list.set(0, 1);
		System.out.println(list);

		/* Delete Elements */

		list.remove(1);
		System.out.println(list);

		/* Size Of ArrayList */

		int size = list.size();
		System.out.println("Getting Size Of List :" + size);
		
		/* Sorting in ArrayList */
		
	    Collections.sort(list);
	    
	    System.out.println(list);
	    
        /*Loops In ArrayList*/
	    
	    for(int i = 0; i<list.size();i++) {
	    	
	    	System.out.println(list.get(i));
	    }
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
